# Angular Module #

## Objetivos ##

Prover um abiente para aplicação angular na porta 4200 através do docker em conjunto com o docker compose.

### Ambiente de Desenvolvimento ###

#### Download e instalação docker-ce ####

* Linux: https://www.digitalocean.com/community/tutorials/como-instalar-e-usar-o-docker-no-ubuntu-16-04-pt

#### Download e instalação docker-compose ####

https://docs.docker.com/compose/install/#install-compose

#### Composing up! ####

No seu diretório de aplicativos, execute:

```bash
docker-compose up -d
```

#### Composing down :( ####

Para encerrar o aplicativo, entre no diretório do aplicativo e execute:

```bash
docker-compose down
```



