import { Cart } from './../_model/cart';
import { Product } from '../_model/product';

export const CART: Cart = { id: 1, quantity: 0, product: [] };

export const PRODUCTS: Product[] = [
  { id: 1, name: 'Mario', price: 'R$ 189,00', oldPrice: 'R$ 220,00' },
  { id: 2, name: 'Homem Aranha', price: 'R$ 189,00', oldPrice: 'R$ 220,00' },
  { id: 3, name: 'Mulher Gato', price: 'R$ 189,00', oldPrice: 'R$ 220,00' },
  { id: 4, name: 'Superman', price: 'R$ 189,00', oldPrice: 'R$ 220,00' },
  { id: 5, name: 'Naruto', price: 'R$ 189,00', oldPrice: 'R$ 220,00' }
];
