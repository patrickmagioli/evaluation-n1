import { Component, OnInit, EventEmitter, Output } from '@angular/core';
import { CartService } from '../_services/cart.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.css']
})
export class ProductComponent implements OnInit {

  constructor(private router: Router, private cartService: CartService) { }

  public modal = true;

  ngOnInit() {
  }

  onPurchase() {
    this.modal = !this.modal;
    this.router.navigate(['/'], { queryParams: { cart: true}});
  }
}
