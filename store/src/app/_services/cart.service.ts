import { Injectable } from '@angular/core';
import { Cart } from '../_model/cart';
import { CART } from '../_data/mock-store';
import { Observable, of } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class CartService {

  constructor() { }

  getCart(): Observable<Cart> {
    return of(CART);
  }

  incrementCart() {
    return of(CART).subscribe(
      data => {
        return data.quantity + 1;
      }
    );
  }
}
