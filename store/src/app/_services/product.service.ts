import { Injectable } from '@angular/core';
import { PRODUCTS } from '../_data/mock-store';
import { Product } from '../_model/product';

@Injectable({
  providedIn: 'root'
})
export class ProductService {

  private products: Product[] = PRODUCTS;
  private searchTerm: boolean;

  constructor() { }

  productsFilter(val: string) {
    if (val === '') {
      return [];
    } else {
      return this.products.map(data => data.name).filter(data =>
        data.toLowerCase().includes(val.toLowerCase()));
    }
  }
}
