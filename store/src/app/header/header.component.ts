import { Component, OnInit, Input } from '@angular/core';
import { CartService } from '../_services/cart.service';
import { FormControl } from '@angular/forms';
import { Observable } from 'rxjs';
import { map, startWith, debounceTime, endWith } from 'rxjs/operators';
import { ActivatedRoute } from '@angular/router';
import { ProductService } from '../_services/product.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  public quantity: number;
  myControl: FormControl = new FormControl();
  filteredOptions;
  searchTerm: boolean;

  constructor(
      public cartService: CartService,
      private productService: ProductService,
      private route: ActivatedRoute
    ) {
      this.route.queryParams.subscribe(params => {
          params.cart ? this.quantity = this.quantity + 1 : this.quantity = 0;
      });
    }

  ngOnInit() {
    this.cartService.getCart().subscribe(
      data => {
        this.quantity = data.quantity;
      }
    );

    this.filteredOptions = this.myControl.valueChanges
      .pipe(
        map(search => this.productService.productsFilter(search))
      );
  }
}
